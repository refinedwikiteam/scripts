#!/bin/bash
#================================================================
# Let's Encrypt renewal script for Apache on Ubuntu/Debian
#================================================================
LOGFILE=/var/log/le-renew.log;
EMAIL=$1;

if [ -z "$EMAIL" ]; then
	echo "[INFO] No email set, logging to console";
else
	exec &>> $LOGFILE;
fi

touch $LOGFILE;

function send_email {
	if [ -s $LOGFILE ]; then
		cat "$LOGFILE" | mail -s "[INFO] [`hostname`] Renewal completed" -a "From: `hostname` <`whoami`@`hostname`>" $EMAIL
		rm -f $ERROR_LOG;
		rm -f $LOGFILE;
	fi;
}
trap send_email EXIT

certbot renew